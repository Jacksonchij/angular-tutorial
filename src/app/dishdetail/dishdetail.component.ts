import { Component, OnInit, Inject } from '@angular/core';
import {Dish} from "../shared/dish";
import {DISHES} from "../shared/dishes";
import {DishService} from "../services/dish.service";

import {Params, ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Feedback, ContactType} from "../shared/feedback";
import {Comment} from "../shared/comment";

import 'rxjs/add/operator/switchMap'
import {expand, flyInOut, visibility} from "../animations/app.animation";


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host:{
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishcopy = null;
  dishIds: number[];
  prev: number;
  next: number;
  commentForm: FormGroup;
  comment: Comment;
  errMess: string;
  visibility = 'shown';

  constructor(private dishservice: DishService,
              private route: ActivatedRoute,
              private location: Location,
              private fb: FormBuilder,
              @Inject('BaseURL') protected BaseURL) {
    this.createForm();
  }

  ngOnInit() {
    this.dishservice.getDishIds()
      .subscribe(dishIds => this.dishIds = dishIds);

    this.route.params
      .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => { this.dish = null; this.errMess = <any>errmess; });

  }

  validationMessages = {
    'author':{
      'required':      'Author Name is required.',
      'minlength':     'Author Name must be at least 2 characters long.'
    },
    'comment':{
      'required':      'Comment is required.',
    }
  }

  formErrors = {
    'author': '',
    'comment': ''
  };

  createForm() {
    this.commentForm = this.fb.group({
      author:['', [Validators.required, Validators.minLength(2)] ],
      rating:['0'],
      comment:['', Validators.required]
    });

    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  setPrevNext(dishId: number){
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];

  }

  onSubmit() {
    this.comment = this.commentForm.value;
    var d = new Date();
    var n = d.toISOString();
    this.comment.date = n;
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save()
      .subscribe(dish => {this.dish = dish; console.log(this.dish);});
    this.resetForm();
  }

  onValueChanged(data?: any){
    if(!this.commentForm){ return;}
    const form = this.commentForm;
    for(const field in this.formErrors){
      this.formErrors[field] = '';
      const control = form.get(field);
      if(control && control.dirty && !control.valid){
        const message = this.validationMessages[field];
        for(const key in control.errors){
          this.formErrors[field] += message[key] + ' ';
        }
      }
    }
  }


  goBack(): void {
    this.location.back();
  }

  resetForm(){
    this.commentForm.reset({
      author: '',
      rating: '5',
      comment: ''
    });
  }

  reset(): void{
    this.resetForm();
    this.comment = new Comment();
  }


}
