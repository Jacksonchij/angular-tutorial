import { Injectable } from '@angular/core';
import {Feedback} from "../shared/feedback";
import {Observable} from "rxjs/Observable";
import {RestangularModule, Restangular} from "ngx-restangular";
import { baseURL } from '../shared/baseurl';
import {Dish} from "../shared/dish";

@Injectable()
export class FeedbackService {

  constructor(private restangular: Restangular) { }

  submitFeedback(feedback : Feedback):Observable<Feedback>{
    return this.restangular.all('feedback').post(feedback);
  }

}
