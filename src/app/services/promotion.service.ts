import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';
import {Observable} from "rxjs/Observable";

import 'rxjs/add/operator/toPromise'
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of'
import {Restangular} from "ngx-restangular";
import {ProcessHttpmsgService} from "./process-httpmsg.service";

@Injectable()
export class PromotionService {

  constructor(private restangular: Restangular,
              private processHTTPMsgService: ProcessHttpmsgService) { }

  getPromotions(): Observable<Promotion[]> {
    return this.restangular.all('promotions').getList();
  }

  getPromotion(id: number):  Observable<Promotion> {
    return this.restangular.all('promotions').getList({featured: true}).map(promotions => promotions[0]);
  }

  getFeaturedPromotion():  Observable<Promotion> {
    return  Observable.of(PROMOTIONS.filter((promotion) => promotion.featured)[0]).delay(2000);
  }
}
